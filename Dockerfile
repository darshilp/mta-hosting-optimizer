FROM ubuntu:16.04
MAINTAINER "darshil.pandya@gmail.com"
WORKDIR /app

RUN apt-get update -y ; apt-get install -y python-pip python-dev ; python -m pip install flask

COPY main.py test_main.py /app/

EXPOSE 5000

ENTRYPOINT [ "python" ]
CMD [ "main.py" ]