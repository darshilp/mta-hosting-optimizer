# Introduction:

This project aims to provide a service that uncovers the inefficient servers therefore hosting only few active mail transfer agents (MTAs).


# Description:

1. Programming Language used: Python 

2. Need to set below environment variables:
```
a) Let 'X' be an integer variable that define the number of active MTAs.
        Default, X="1"
b) URL of mock service used to provide sample IP configuration data.
        Default, url="http://ip-config:8080/ip-configs"
```         
3. Exposes REST endpoint to retrieve hostnames having less or equals X active IP addresses exists as below.<br/>
```
Endpoint: http://127.0.0.1:5000/
```
4. `Docker` is used for packaging and shipping of our application.

# Installation:

## For testing and development purposes.

``` 
Build :
        $ docker build -t mta-hosting-optimizer .
Test:
        $ python test_main.py
Deploy:
        $ docker-compose up -d
Coverage:        
        $ coverage run main.py
        $ coverage report	
```

## GitLab CI/CD

* Auto-build and Auto-deploy is achieved with ".gitlab-ci.yml".
* Refer the above file to have a look at stages mentioned.

## Rancher

`docker-compose.yaml` and `rancher-compose.yaml` is present under `rancher` directory to deploy our application on Rancher platform with production grade features such as below:<br/>
```
a) At least 2 instances of the application are running at the same time.
b) A load balancer is present.
c) Only one service container of the application must run per host.
```       

