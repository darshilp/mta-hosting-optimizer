import os
import urllib, json
from flask import Flask
from flask import jsonify

X = int(os.getenv("X", 1))
url = os.environ.get("url", "http://localhost:8080/ip-configs")

app = Flask(__name__)

def request():
        response = urllib.urlopen(url)
        if response.getcode() == 200:
           data = json.loads(response.read())
           return data, response
        else:
           print('Unable to access endpoint.')


@app.route("/")
def get_inactive_hosts():
        data, response =  request()

        list = {}
        for d in data:
            list.setdefault(d['hostname'], []).append(d['active'])

        output = []
        for hostname,activeStatus in list.items():
            if sum(activeStatus) <= X:
              output.append(hostname)
        return jsonify(output)

if __name__ == "__main__":
    app.run()