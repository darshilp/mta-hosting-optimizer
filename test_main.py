import unittest
from main import app

class BasicTests(unittest.TestCase):
    def test_response_and_validate_data(self):
        test = app.test_client(self)
        response = test.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'["mta-prod-3","mta-prod-1"]\n')

if __name__ == "__main__":
    unittest.main()
